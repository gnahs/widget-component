class StoreDates {
    static set(dates) {
        this.setLocalStorage(dates)
    }
    // eslint-disable-next-line
    setLocalStorage(dates) {
        sessionStorage.setItem('gna-booking-dates', JSON.stringify(dates))
    }
    // eslint-disable-next-line
    get() {
        const dates = sessionStorage.getItem('gna-booking-dates')
        return JSON.parse(dates)
    }

    static checkIn() {
        const dates = JSON.parse(sessionStorage.getItem('gna-booking-dates'))
        if (dates === null) {
            return null
        }

        return dates.checkIn
    }

    static checkOut() {
        const dates = JSON.parse(sessionStorage.getItem('gna-booking-dates'))
        if (dates === null) {
            return null
        }

        return dates.checkOut
    }
}

export default StoreDates
