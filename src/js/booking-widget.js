/* eslint-disable eqeqeq */
/* eslint-disable max-len */
import DatesComponent from 'dates-component/src/js/dates-component'
import DestinationComponent from 'destination-component/src/js/destination-component'
import OccupancyComponent from 'occupancy-component/src/js/occupancy-component'
import Merge from 'deepmerge'
import i18n from './i18n'
import { h } from 'utils/utils'
import {
    Append,
    CustomEvent as CustomEventPollyfill,
    ForEach,
    Includes,
    Matches,
    Preppend,
} from 'js-polyfills/src/js-polyfills'
import {
    arrowDown,
    calendarIcon,
    placeIcon,
    roomsIcon,
} from './svg'
import FixBookingWidget from './fix-booking-widget'
import Serialize from './serialize'
import StoreDates from './store-dates'
import { LastSearch } from './last-search'
import { Settings } from './settings'

class BookingWidget {
    constructor(params) {
        this.selectors = {
            dates: '.c-booking-widget .dates-component',
            destination: '.c-booking-widget .destination-component',
            occupancy: '.c-booking-widget .occupancy-component',
            promocode: '.c-booking-widget .promo-code',
            submit: '.c-booking-widget .booking-button',
            fixedTo: '.c-booking-widget',
        }

        this.settings = {
            appeareance: {
                promoCodeIcon: {
                    show: false,
                    iconStr: `
                        <svg class="c-promo-info-icon" xmlns="https://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13">
                            <defs><style>.a{fill:#6f2277;}</style></defs>
                            <path class="a" d="M6.5,0A6.5,6.5,0,1,0,13,6.5,6.507,6.507,0,0,0,6.5,0Zm0,11.818A5.318,5.318,0,1,1,11.818,6.5,5.324,5.324,0,0,1,6.5,11.818Z"/>
                            <path class="a" d="M145.871,70a.869.869,0,1,0,.869.869A.87.87,0,0,0,145.871,70Z" transform="translate(-139.371 -67.282)"/>
                            <path class="a" d="M150.652,140a.652.652,0,0,0-.652.652v3.91a.652.652,0,1,0,1.3,0v-3.91A.652.652,0,0,0,150.652,140Z" transform="translate(-144.152 -134.809)"/>
                        </svg>
                    `,
                },
            },
            saveLastSearch: false,
        }
        this.userSelection = {
            dates: {},
        }

        if (!params.settings.uuid) {
            window.i18n_widget = i18n[params.settings.language]

            this.configuration = params.configuration || {}
            this.configuration.destinations = params.settings.destinations

            // Object.assign(this.selectors, params.selectors)
            if (typeof params.selectors !== 'undefined') {
                this.selectors = Merge(this.selectors, params.selectors)
            }
            this.settings = Merge(this.settings, params.settings)

            const savedBookingData = LastSearch.getData()

            this.init()

            const event = new CustomEvent('initWidget', {})
            document.querySelector('.c-booking-widget').dispatchEvent(event) 
                 
            // eslint-disable-next-line no-new
            new FixBookingWidget(this.selectors.fixedTo, { gap: this.settings.gap, appeareance: this.settings.appeareance })
            if (this.settings.saveLastSearch && this.isBookingDataValid(savedBookingData) && savedBookingData !== null) {
                this.restoreBookingData(savedBookingData)
            } else {
                LastSearch.removeData()
            }
            return
        }

        let apiSettings = {}
        Settings.getData(params.settings).then(settings => {
            apiSettings = settings

            window.i18n_widget = i18n[params.settings.language]

            this.configuration = params.configuration || {}
            this.configuration = this.parseSettings(apiSettings, params.settings)

            if (this.configuration.destinations.length > 1) {
                this.configuration.destinations.unshift({
                    id: 0,
                    type: 'establishment',
                    translation_type: 'establishment',
                    establishment_type: 'hotel',
                    name: window.i18n_widget['all'],
                })
            }
            
            if (typeof params.selectors !== 'undefined') {
                this.selectors = Merge(this.selectors, params.selectors)
            }
            this.settings = Merge(this.settings, params.settings)

            const savedBookingData = LastSearch.getData()

            const eventBefore = new CustomEvent('beforeInitWidget', {})
            document.querySelector('.c-booking-widget').dispatchEvent(eventBefore)

            this.init()
            
            const event = new CustomEvent('initWidget', {})
            document.querySelector('.c-booking-widget').dispatchEvent(event)
            
            // eslint-disable-next-line no-new
            new FixBookingWidget(this.selectors.fixedTo, { gap: this.settings.gap, appeareance: this.settings.appeareance })
            if (this.settings.saveLastSearch && this.isBookingDataValid(savedBookingData) && savedBookingData !== null) {
                this.restoreBookingData(savedBookingData)
            } else {
                LastSearch.removeData()
            }
        })
    }

    init() {
        this.initSelectors()
        this.mountDOMDestination()
        this.mountDOMDates()
        this.mountDOMOccupancy()
        this.mountDOMPromocode()
        this.mountDOMSubmit()
        this.mountDOMResume()
        this.bindEvents()
        this.initDestinations()
        this.destinationEvents()
        this.initDates(this.configuration.dates)
        this.initOccupancy(this.configuration.occupancy)
        this.show()
        this.setCustomProperties()
    }

    show() {
        document.querySelector(this.selectors.fixedTo).classList.add('c-booking-widget__loadded')
    }

    parseSettings(apiSettings, params) {
        const configuration = {}
        configuration.destinations = apiSettings.destinations
        configuration.dates = apiSettings.client.dates
        configuration.occupancy = apiSettings.client

        return configuration
    }

    initSelectors() {
        this.$destination = null
        this.$dates = null
        this.$occupancySelector = null
        this.$promocode = document.querySelector('.c-promo')

        if (Object.prototype.hasOwnProperty.call(this.settings, 'appeareance') && Object.prototype.hasOwnProperty.call(this.settings.appeareance, 'condensed')) {
            this.$widgetContainer = document.querySelector(`${this.selectors.fixedTo} .c-booking-widget__container`)
            this.$widgetContainer.classList.add('condensed')
        }
    }

    initDestinations() {
        const selector = document.querySelector('.destination-component')
        if (selector === null) return

        const defaultSettings = { destinations: this.configuration.destinations }

        if (this.settings.appeareance && this.settings.appeareance.destinations) {
            defaultSettings.appeareance = this.settings.appeareance.destinations
        }
        const destinationsSettings = Object.prototype.hasOwnProperty.call(this.configuration, 'destinations') ? Merge(this.configuration.destinations, defaultSettings) : defaultSettings
        this.$destination = new DestinationComponent(this.selectors.destination, destinationsSettings)

        const destinationContainers = document.querySelectorAll('.destination-component')
        destinationContainers.forEach((destinationContainer) => {
            const id = destinationContainer.getAttribute('id')
            if (id === null || id.indexOf('destination-component-') === -1) return
            const label  = selector.getAttribute('widget-label-destination') !== null
                ? selector.getAttribute('widget-label-destination') : this.settings.destinationLabel
            const destinationLabel = h('div', { class: 'destination-component__name' }, label)
            const parent = destinationContainer.parentNode
            const destinationWrapper = h('div', { class: 'destination-component__wrapper destination-component' }, destinationLabel, destinationContainer)
            parent.prepend(destinationWrapper)
        })
    }

    initDates(apiParams) {
        const selector = document.querySelector(this.selectors.dates)
        if (selector === null) return

        const isIE11 = !!window.MSInputMethodContext && !!document.documentMode

        const defaultSettings = {
            setFormat: 'YYYY-MM-DD',
            locale: {
                language: this.settings.language,
                format: 'YYYY-MM-DD',
                culture: this.settings.language,
            },
            dates: {
                checkIn: !isIE11 ? StoreDates.checkIn() : null,
                checkOut: !isIE11 ? StoreDates.checkOut() : null,
                start: apiParams.check_in ? new Date(apiParams.check_in.min) || null : null,
                end: null,
                checkInMin: apiParams.check_in ? new Date(apiParams.check_in.min) || null : null,
                checkInMax: apiParams.check_in ? new Date(apiParams.check_in.max) || null : null,
                nightsMin: apiParams.nights ? apiParams.nights.min || 1 : 1,
                nightsMax: apiParams.nights ? apiParams.nights.max || null : null
            },
            appeareance: {
                mode: 'custom',
                monthsShowed: 2,
                checkInSelector: '.check-in-selector',
                checkOutSelector: '.check-out-selector',
                offset: {
                    checkInTop: -1,
                    checkInLeft: -1,
                    checkOutTop: -1,
                    checkOutLeft: -1,
                },
            },
        }

        const datesSettings = Object.prototype.hasOwnProperty.call(this.configuration, 'dates') ? Merge(defaultSettings, this.configuration.dates) : defaultSettings
        this.$dates = new DatesComponent(this.selectors.dates, datesSettings)
    }

    initOccupancy(apiParams) {
        const selector = document.querySelector('.occupancy-component-container')
        if (selector === null) return

        let defaultSettings = {}
        if (this.settings.uuid) {
            defaultSettings = {
                locale: {
                    language: this.settings.language,
                },
                appeareance: {
                    mode: 'custom',
                    occupancySelector: '.custom-selector',
                },
                occupancy: {
                    establishment:apiParams.type,
                    roomsMax: apiParams.occupancy.lodges,
                    adultsMin: apiParams.occupancy.people.adults.min,
                    adultsMax: apiParams.occupancy.people.adults.max,
                    adultsDefault: apiParams.occupancy.people.adults.default,
                    childMin: apiParams.occupancy.people.children.min,
                    childMax: apiParams.occupancy.people.children.max,
                    childDefault: apiParams.occupancy.people.children.default,
                    childAgeMin: apiParams.occupancy.people.children.ages.min,
                    childAgeMax: apiParams.occupancy.people.children.ages.max,
                    childAgeDefault: apiParams.occupancy.people.children.ages.default,
                    divideAdultsAndChild: apiParams.occupancy.people.adults_children_separated,
                    adultsOnly: apiParams.occupancy.people.adults_only,
                }
            }
        }
        
        const occupancySettings = Object.prototype.hasOwnProperty.call(this.configuration, 'occupancy') ? Merge(defaultSettings, this.configuration.occupancy) : defaultSettings
        this.$occupancy = new OccupancyComponent(this.selectors.occupancy, occupancySettings)
    }

    // ----------------------
    // MOUNT DOM FUNCTIONS
    // ----------------------
    mountDOMDestination() {
        const destinationContainer = document.querySelector('.destination-component')
        if (typeof destinationContainer === 'undefined' || destinationContainer === null) return
        const input = h('input', { class: 'destination-selector', type: 'text' })
        const defaulDestinationLabel = this.settings.destinationLabel ? this.settings.destinationLabel : window.i18n_widget['destination']
        const label  = destinationContainer.getAttribute('widget-label-destination') !== null
            ? destinationContainer.getAttribute('widget-label-destination') : defaulDestinationLabel
        const destinationLabel = h('div', { class: 'destination-component__name' }, label)
        destinationContainer.append(destinationLabel, input)
    }

    mountDOMDates() {
        this.createCheckInDOMtemplate()
        this.createCheckOutDOMtemplate()
    }

    createCheckInDOMtemplate() {
        const dateContainer = document.querySelector(this.selectors.dates)
        const defaultCheckInLabel = this.settings.checkInLabel ? this.settings.checkInLabel : window.i18n_widget['checkin_date']
        const label = dateContainer.getAttribute('widget-label-check-in') !== null
            ? dateContainer.getAttribute('widget-label-check-in') : defaultCheckInLabel
        const checkLabel = h('div', { class: 'check-name' }, label)
        const dayNumber = h('div', { class: 'day-number' })
        const month = h('div', { class: 'month' })
        const year = h('div', { class: 'year' })
        const day = h('div', { class: 'day' })
        const wrapper = h('div', { class: 'widget-wrapper' }, month, year, day)
        const checkInSelector = h('div', { class: 'check-in-selector' }, dayNumber, wrapper)
        checkInSelector.insertAdjacentHTML('beforeend', arrowDown)
        const checkInContainer = h('div', { class: 'check-in-container' }, checkLabel, checkInSelector)
        dateContainer.append(checkInContainer)
    }

    createCheckOutDOMtemplate() {
        const dateContainer = document.querySelector(this.selectors.dates)
        const defaultCheckOutLabel = this.settings.checkOutLabel ? this.settings.checkOutLabel : window.i18n_widget['checkout_date']
        const label = dateContainer.getAttribute('widget-label-check-out') !== null
            ? dateContainer.getAttribute('widget-label-check-out') : defaultCheckOutLabel
        const checkLabel = h('div', { class: 'check-name' }, label)
        const dayNumber = h('div', { class: 'day-number' })
        const month = h('div', { class: 'month' })
        const year = h('div', { class: 'year' })
        const day = h('div', { class: 'day' })
        const wrapper = h('div', { class: 'widget-wrapper' }, month, year, day)
        const checkOutSelector = h('div', { class: 'check-out-selector' }, dayNumber, wrapper)
        checkOutSelector.insertAdjacentHTML('beforeend', arrowDown)
        const checkOutContainer = h('div', { class: 'check-out-container' }, checkLabel, checkOutSelector)
        dateContainer.append(checkOutContainer)
    }

    mountDOMOccupancy() {
        const occupancyContainer = document.querySelector('.occupancy-component-container')
        const defaultOccupancyLabel = this.settings.occupancyLabel ? this.settings.occupancyLabel : window.i18n_widget['rooms_and_occupancy']
        const label = occupancyContainer.getAttribute('widget-label-occupancy') !== null
            ? occupancyContainer.getAttribute('widget-label-occupancy') : defaultOccupancyLabel
        const occupancyLabel = h('div', { class: 'occupancy-name' }, label)
        const occupancyCustomSelector = h('div', { class: 'custom-selector' })
        const checkOutContainer = h('div', { class: 'occupancy-component' }, occupancyCustomSelector)
        occupancyContainer.append(occupancyLabel, checkOutContainer)
        occupancyContainer.insertAdjacentHTML('beforeend', arrowDown)
    }

    mountDOMPromocode() {
        const promocodeContainer = document.querySelector(this.selectors.promocode)
        if (typeof promocodeContainer === 'undefined' || promocodeContainer === null) return
        
        const defaultPromocodeLabel = this.settings.promocodeLabel ? this.settings.promocodeLabel : ''
        const label = promocodeContainer.getAttribute('widget-label-promocode') !== null
            ? promocodeContainer.getAttribute('widget-label-promocode') : defaultPromocodeLabel
        const input = h('input', {
            class: 'c-promo',
            type: 'text',
            placeholder: label,
            'aria-label': label,
        })
        this.$promocode = input
        
        const promocodeLabel = h('div', { class: 'promo-code__name' }, label)
        if (Object.keys(this.settings.appeareance).length
            && Object.keys(this.settings.appeareance.promoCodeIcon).length
            && this.settings.appeareance.promoCodeIcon.show) {
            const event = new CustomEvent('promoInfoIcon', {})
            const infoIconDOM = new DOMParser().parseFromString(this.settings.appeareance.promoCodeIcon.iconStr, 'text/html').body.children[0]
            promocodeLabel.append(infoIconDOM)
            infoIconDOM.addEventListener('click', () => {
                infoIconDOM.dispatchEvent(event)
            })
        }
        promocodeContainer.append(promocodeLabel, input)
    }

    mountDOMSubmit() {
        const bookingButtonContainer = document.querySelector(this.selectors.submit)
        const defaultBookingLabel = this.settings.bookingButtonLabel ? this.settings.bookingButtonLabel : window.i18n_widget['book']
        const label = bookingButtonContainer.getAttribute('widget-label-booking') !== null
            ? bookingButtonContainer.getAttribute('widget-label-booking') : defaultBookingLabel
        const button = h('button', { class: 'c-button', type: 'button' }, label)
        this.$submitBooking = button
        bookingButtonContainer.append(button)
    }
    // eslint-disable-next-line
    mountDOMResume() {
        const bookingButtonContainer = document.querySelector(this.selectors.submit)
        if (Object.prototype.hasOwnProperty.call(this.settings, 'appeareance')
            && Object.prototype.hasOwnProperty.call(this.settings.appeareance, 'scrollHide')) {
            return
        }
        const resume = h('div', { class: 'c-booking-widget__resume' })
        const resumeDates = h('div', { class: 'c-booking-widget__resume__dates' })
        resumeDates.append(h('div', { class: 'c-booking-widget__resume__check-in' }), h('div', { class: 'c-booking-widget__resume__check-out' }))

        const resumeInfo = h('div', { class: 'c-booking-widget__resume__info' })
        const destinationContainer = document.querySelector('.destination-component')
        if (typeof destinationContainer !== 'undefined' && destinationContainer !== null) {
            resumeInfo.append(h('div', { class: 'c-booking-widget__resume__destination' }))
        }
        resumeInfo.append(resumeDates, h('div', { class: 'c-booking-widget__resume__occupancy' }), h('div', { class: 'c-booking-widget__resume__promocode' }))

        const bookingButton = h('div', { class: 'c-booking-widget__resume__button booking-button' })
        const label = bookingButtonContainer.getAttribute('widget-label-booking') !== null
            ? bookingButtonContainer.getAttribute('widget-label-booking') : this.settings.bookingButtonLabel
        const button = h('button', { class: 'c-button', type: 'button' }, label)
        bookingButton.append(button)

        resume.append(resumeInfo, bookingButton)
        document.querySelector('.c-booking-widget__body').append(resume)
    }

    // ----------------------
    // EVENTS
    // ----------------------
    bindEvents() {
        this.datesEvents()
        this.occupancyEvents()
        this.bookingEvent()
    }

    bookingEvent() {
        this.$submitBooking.addEventListener('click', (ev) => {
            ev.preventDefault()
            ev.stopPropagation()
            if (this.$promocode) {
                this.userSelection.promoCode = this.$promocode.value
            }
            const url = new Serialize(this.userSelection)
            window.location.href = this.settings.bookingRoute + url.encode()
        })
    }

    destinationEvents() {
        if (this.$destination === null) return
        this.$destination.selector().addEventListener('selector:changed', (ev) => {
            // eslint-disable-next-line prefer-destructuring
            let name = ev.detail.name
            // eslint-disable-next-line
            if (!ev.detail.hasOwnProperty('name')) {
                // eslint-disable-next-line
                this.configuration.destinations.forEach(dest => {
                    if (dest.id === parseInt(ev.detail.id, 10)) {
                        name = dest.name
                        return
                    }
                    if (dest.hasOwnProperty('children')) {
                        dest.children.forEach((child) => {
                            if (child.id === parseInt(ev.detail.id, 10)) {
                                name = child.name
                                return
                            }
                            if (child.hasOwnProperty('children')) {
                                child.children.forEach((secondchild) => {
                                    if (secondchild.id === parseInt(ev.detail.id, 10)) {
                                        name = secondchild.name
                                    }
                                })
                            }
                        })
                    }
                })
                // name = this.settings.destinations.filter((dest) => { return dest.id === parseInt(ev.detail.id, 10) })[0].name
            }
            this.userSelection.destination = ev.detail.id
            this.userSelection.destination_type = ev.detail.type
            // eslint-disable-next-line arrow-body-style
            if (Object.prototype.hasOwnProperty.call(this.settings, 'appeareance')
                && !Object.prototype.hasOwnProperty.call(this.settings.appeareance, 'scrollHide')) {
                document.querySelector('.c-booking-widget__resume__destination').innerHTML = `${placeIcon} <span>${name}</span>`
            }

            this.onBookingDataChange()
        })
        // this.$destination.setDestination(this.settings.destinations[0])
    }

    datesEvents() {
        // eslint-disable-next-line prefer-const
        let selector = document.querySelector(this.selectors.dates)
        if (selector === null) return
        selector.addEventListener('checkIn', (ev) => {
            selector.querySelector('.check-in-selector .day-number').innerHTML = ev.detail.dayOfMonth
            selector.querySelector('.check-in-selector .day').innerHTML = ev.detail.dayOfWeek
            selector.querySelector('.check-in-selector .month').innerHTML = `${ev.detail.month}, ${ev.detail.year}`
            if (Object.prototype.hasOwnProperty.call(this.settings, 'appeareance')
                && !Object.prototype.hasOwnProperty.call(this.settings.appeareance, 'scrollHide')) {
                document.querySelector('.c-booking-widget__resume__check-in').innerHTML = `${calendarIcon} <span>${ev.detail.dayOfMonth} ${ev.detail.month}, ${ev.detail.year}</span>`
            }
            this.userSelection.dates.checkIn = ev.detail.format
        }, false)

        selector.addEventListener('checkOut', (ev) => {
            selector.querySelector('.check-out-selector .day-number').innerHTML = ev.detail.dayOfMonth
            selector.querySelector('.check-out-selector .day').innerHTML = ev.detail.dayOfWeek
            selector.querySelector('.check-out-selector .month').innerHTML = `${ev.detail.month}, ${ev.detail.year}`
            if (Object.prototype.hasOwnProperty.call(this.settings, 'appeareance')
                && !Object.prototype.hasOwnProperty.call(this.settings.appeareance, 'scrollHide')) {
                document.querySelector('.c-booking-widget__resume__check-out').innerHTML = `${calendarIcon} <span>${ev.detail.dayOfMonth} ${ev.detail.month}, ${ev.detail.year}</span>`
            }
            this.userSelection.dates.checkOut = ev.detail.format
            this.onBookingDataChange()
        }, false)

        selector.addEventListener('openCheckIn', (ev) => {
            selector.querySelector('.check-in-selector').classList.add('opened')
        }, false)

        selector.addEventListener('openCheckOut', (ev) => {
            selector.querySelector('.check-out-selector').classList.add('opened')
        }, false)

        selector.addEventListener('closeDatePicker', (ev) => {
            selector.querySelector('.check-in-selector').classList.remove('opened')
            selector.querySelector('.check-out-selector').classList.remove('opened')
        }, false)

        // eslint-disable-next-line consistent-return
        return this
    }

    onBookingDataChange() {
        if (!this.settings.saveLastSearch) return

        const {
            dates, occupancy, destination, destination_type: destinaitonType,
        } = this.userSelection
        const destinatonData = destination
            ? {
                id: parseInt(destination, 10),
                type: destinaitonType,
            }
            : null
        const formattedData = LastSearch.formatDataToSave(dates.checkIn, dates.checkOut, occupancy, destinatonData)
        LastSearch.save(formattedData)
    }

    /**
     *
     * @param {import('./last-search').BookingData|null} bookingData
     * @returns {boolean}
     */
    // eslint-disable-next-line class-methods-use-this
    isBookingDataValid(bookingData) {
        if (!bookingData) return false
        const isDestinationValid = bookingData.destination !== null 
            ? this.$destination?.isValid(bookingData.destination) 
            : true

        const isDatesValid = this.$dates?.isValid(new Date(bookingData.checkIn), new Date(bookingData.checkOut))
        const isOccupancyValid = this.$occupancy?.isValid(bookingData.occupancy)

        return isDestinationValid && isDatesValid && isOccupancyValid
    }

    /**
     * @param {import('./last-search').BookingData} bookingData
     */
    restoreBookingData(bookingData) {
        if (bookingData.destination) {
            this.$destination?.setDestination(bookingData.destination)
        }
        this.$dates?.setDates(new Date(bookingData.checkIn), new Date(bookingData.checkOut))
        this.$occupancy?.setOccupancy(bookingData.occupancy)
    }

    occupancyEvents() {
        const selector = document.querySelector(this.selectors.occupancy)
        if (selector === null) return
        selector.addEventListener('occupancy', (ev) => {
            document.querySelector('.occupancy-component-container').classList.remove('opened')
            this.userSelection.occupancy = ev.detail.occupancy
            document.querySelector('.custom-selector').innerHTML = ev.detail.humanized
            if (Object.prototype.hasOwnProperty.call(this.settings, 'appeareance')
                && !Object.prototype.hasOwnProperty.call(this.settings.appeareance, 'scrollHide')) {
                document.querySelector('.c-booking-widget__resume__occupancy').innerHTML = `${roomsIcon} <span>${ev.detail.humanized}</span>`
            }
            this.onBookingDataChange()
        }, false)

        selector.addEventListener('openOccupancy', (ev) => {
            document.querySelector('.occupancy-component-container').classList.add('opened')
        })
    }

    setCustomProperties() {
        if (!Object.hasOwnProperty.call(this.settings.appeareance, 'colors')) return
        let cssContent = ':root {'
        // eslint-disable-next-line no-restricted-syntax
        for (const [ key, value ] of Object.entries(this.settings.appeareance.colors)) {
            cssContent += `--booking-${key}: ${value};`
        }
        cssContent += '}'

        const body = document.querySelector('body')
        const style = h('style', {}, cssContent)

        body.appendChild(style)
    }
}

export default BookingWidget
