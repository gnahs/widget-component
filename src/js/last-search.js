/**
 * @typedef {Object} Occupancy
 * 
 * @property {number} adults
 * @property {number[]} children
 */

/**
 * @typedef {Object} Destination
 * 
 * @property {number} id
 * @property {string} type
 */

/**
 * @typedef {Object} BookingData
 * 
 * @property {Destination|null} destination
 * @property {string} checkIn
 * @property {string} checkOut
 * @property {Occupancy} occupancy
 */

export class LastSearch {
    static LOCALE_STORAGE_KEY = 'GNAHS-booking-widget-last-search'

    /**
     * @param {string} checkIn
     * @param {string} checkOut
     * @param {Occupancy} occupancy
     * @param {null|Destination=} destination
     * @returns {BookingData}
     */
    static formatDataToSave(checkIn, checkOut, occupancy, destination = null) {
        const bookingData = {
            checkIn,
            checkOut,
            occupancy,
            destination
        }

        return bookingData
    }

    /**
     * @param {BookingData} bookingData 
     */
    static save(bookingData) { 
        const dataToSave = JSON.stringify(bookingData)
        localStorage.setItem(LastSearch.LOCALE_STORAGE_KEY, dataToSave)
    }

    /**
     * @returns {null|BookingData}
     */
    static getData() {
        const bookingData = localStorage.getItem(LastSearch.LOCALE_STORAGE_KEY)
        return bookingData 
            ? JSON.parse(bookingData)
            : null
    }

    static removeData() {
        localStorage.removeItem(LastSearch.LOCALE_STORAGE_KEY)
    }
}