/* eslint-disable */
class Serialize {

    constructor(params) {
        this.params = params
    }

    encode(){
        let response = ''

        response += this.destination()
        response += this.dates()
        response += this.occupancy()
        response += this.promoCode()

        return '?'+response.substr(1)
    }

    decode(){

    }

    destination(){

        if(!this.params.hasOwnProperty('destination') || this.params.destination === '' || this.params.destination === null){
            return ''
        }
        
        var destination_type = ''
        if(this.params.hasOwnProperty('destination_type') && this.params.destination_type !== '' && this.params.destination_type !== null){
            destination_type = '&destination_type=' + this.params.destination_type
        }

        return '&destination='+this.params.destination + destination_type
    }

    dates(){

        if(!this.params.hasOwnProperty('dates')){
            return ''
        }

        return `&checkin=${this.params.dates.checkIn}&checkout=${this.params.dates.checkOut}`
    }

    occupancy(){

        if(!this.params.hasOwnProperty('occupancy')){
            return ''
        }

        let rooms = `&rooms=${this.params.occupancy.length}`

        this.params.occupancy.forEach((room, index) => {
            index++

            rooms += `&room${index}_adults=${room.adults}`
            if(!room.children.length){
                return
            }

            rooms += `&room${index}_children=${room.children.join(',')}`
        })

        return rooms
    }

    promoCode(){

        if(!this.params.hasOwnProperty('promoCode')){
            return ''
        }

        return `&cpromo=${encodeURIComponent(this.params.promoCode)}`
    }

}

export default Serialize
