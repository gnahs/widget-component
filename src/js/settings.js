export class Settings {
    static getData(bookingParams) {
        const params = {locale: this.getLocale(bookingParams.language), ignore: true, establishments: bookingParams.establishments};
        const cacheKey = `${bookingParams.apiUrl}/api/widget?${JSON.stringify(params)}`
        const cacheName = 'api-cache'

        const storageDate = localStorage.getItem('GNAHS-widget-settings-date')
        let storageDateIsOld = true
        if (storageDate !== null) {
            storageDate.setDate(storageDate.getDate() + 3)
            storageDateIsOld = storageDate < new Date()
        }

        if (!('caches' in window) || storageDateIsOld) {
            return this.fetchApi(bookingParams.apiUrl, bookingParams.uuid, params, cacheKey, cacheName)
        }

        return caches.open(cacheName)
            .then(cache => cache.match(cacheKey))
            .then(response => {
                console.log('cached response')
                if (response) {
                    return response.json().then(res => res.data)
                }

            return this.fetchApi(bookingParams.apiUrl, bookingParams.uuid, params, cacheKey, cacheName)
            })
            .catch(error => {
                console.log('error')
                return this.fetchApi(bookingParams.apiUrl, bookingParams.uuid, params, cacheKey, cacheName)
            })
    }

    static getApiParams(uuid, params) {
        const headers = new Headers()
        headers.append('Access-Control-Allow-Origin', '*')
        headers.append('Accept', 'application/json')
        headers.append('Content-Type', 'application/json')
        headers.append('GNAHS-Auth-Customer', uuid)

        return {
            headers,
            method: 'POST',
            body: JSON.stringify(params),
          }
    }

    static fetchApi(apiUrl, uuid, params, cacheKey, cacheName) {
        return fetch(`${apiUrl}/api/widget`, this.getApiParams(uuid, params))
        .then(response => {
            console.log('api response')
            const clonedResponse = response.clone()
            if ('caches' in window) {
                caches.open(cacheName)
                    .then(cache => cache.put(cacheKey, clonedResponse))
                localStorage.setItem('GNAHS-widget-settings-date', new Date())
            }
            return response.json().then(res => res.data)
        });
    }

    static getLocale(language) {
        if (language === 'ca') {
            return 'ca_ES'
        }

        return `${language}_${language.toUpperCase()}`
    }
}