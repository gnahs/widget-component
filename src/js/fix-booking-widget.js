// eslint-disable-next-line
import { elementPosition, isMobile } from 'utils/utils'

class FixBookingWidget {
    constructor(selector, options = {}) {
        this.element = document.querySelector(`${selector}`)
        if (!this.element) {
            return
        }

        this.options = {
            gap: 0,
            appeareance: {},
        }
        // eslint-disable-next-line
        if (options.hasOwnProperty('gap')) {
            this.options.gap = options.gap
        }
        if (Object.prototype.hasOwnProperty.call(options, 'appeareance')) {
            this.options.appeareance = options.appeareance
        }

        if (Object.prototype.hasOwnProperty.call(this.options.appeareance, 'scrollHide')) return
        if (Object.prototype.hasOwnProperty.call(this.options.appeareance, 'condensed')) return

        this.$ = {}

        this.events = {
            transitionEnd: this.transitionEnd.bind(this),
            mouseOver: this.mouseOver.bind(this),
            mouseMove: this.mouseMove.bind(this),
        }

        this.init()
    }

    init() {
        this.initSelectors()
        this.initAnimationSelectors()
        if (!isMobile()) this.setElementBodyHeightStyle()
        this.bindScroll()
    }

    initSelectors() {
        this.$window = document.querySelector('body')
        this.width = elementPosition(this.$window).width
        this.scrollTop = FixBookingWidget.toScrollTop()
        this.originalPosition = this.scrollTop + this.offsetUp()
        this.elementWidth = elementPosition(this.element).width
        this.elementHeight = elementPosition(this.element).height
        if (!isMobile()) this.setWidgetContainerHeightStyle()
        this.hasFixed = false
        this.isCollapsed = false
        this.mouseIsOverElement = false
    }

    initAnimationSelectors() {
        this.$.elementBody = this.element.querySelector('.c-booking-widget__body')
        this.$.resume = this.$.elementBody.querySelector('.c-booking-widget__resume')
    }

    setWidgetContainerHeightStyle() {
        this.element.querySelector('.c-booking-widget__container').style.height = `${this.elementHeight}px`
    }

    setElementBodyHeightStyle() {
        this.$.elementBody.style['max-height'] = `${this.elementHeight}px`
    }

    static toScrollTop() {
        return window.pageYOffset
        || document.documentElement.scrollTop
        || document.body.scrollTop
        || 0
    }

    bindScroll() {
        window.addEventListener('scroll', (ev) => {
            this.scrollTop = FixBookingWidget.toScrollTop()
            if (this.width >= '1025' && !Object.prototype.hasOwnProperty.call(this.options.appeareance, 'condensed')) this.fix()
        }, false)
    }

    offsetUp() {
        return elementPosition(this.element).top
    }

    hasToFix() {
        if (this.offsetUp() < this.options.gap) return true
        if (this.scrollTop <= this.originalPosition) return false
        return false
        // return (this.scrollTop - this.options.gap >= this.offsetUp)
        // return (this.options.gap >= this.offsetUp)
    }

    fix() {
        if (this.hasToFix()) {
            if (this.scrollTop > (this.offsetUp() + this.options.gap) && this.hasFixed === false) {
                this.emulateContainerHeight()
                // Add fixed class
                this.element.classList.add('fixed')
                this.element.style.top = `${this.options.gap}px`
                // Start to do more shrinked element
                this.element.classList.add('c-booking-widget--shrink')
                // Move up the inside element body
                this.$.elementBody.classList.add('animate')
                // Do visible resume element
                this.$.resume.classList.add('show')
                // Bind event to do grower element
                this.bindMouseoverEvent()
                // Set collapsed and fixed variable
                this.isCollapsed = true
                this.hasFixed = true
                // Fix resume if hooked betwen animations
                setTimeout(() => {
                    if (!this.$.resume.classList.contains('show')) {
                        this.$.resume.classList.add('show')
                    }
                }, 650)
            }
            return
        }

        if (!this.isCollapsed && this.hasFixed && !this.mouseIsOverElement) {
            this.element.classList.remove('c-booking-widget--no-overflow')
            window.setTimeout(() => {
                this.$.resume.classList.add('show')
                this.element.classList.remove('c-booking-widget--shrink-tmp')
                this.$.elementBody.classList.add('animate')
                this.isCollapsed = true
                this.dispatchEvent(false)
            }, 200)
        }

        // eslint-disable-next-line max-len
        if ((this.temporalPosition() > this.options.gap || this.temporalPosition() === 0) && this.hasFixed === true) {
            this.element.classList.remove('fixed')
            this.element.style.top = `0px`
            this.element.classList.add('c-booking-widget--shrink-tmp')
            this.element.classList.remove('c-booking-widget--shrink')
            this.bindAnimationEvent()
            this.$.elementBody.classList.remove('animate')
            this.isCollapsed = false
            this.unBindMouseoverEvent()
            document.querySelector('.c-booking-widget-temporal').remove()
            this.hasFixed = false
        }
    }

    // eslint-disable-next-line class-methods-use-this
    temporalPosition() {
        const selector = document.querySelector('.c-booking-widget-temporal')
        if (selector !== null) return elementPosition(selector).top
        return 0
    }

    dispatchEvent(action) {
        const event = new CustomEvent('widget-positioned', {
            detail: {
                grow: action,
            },
        })
        this.element.dispatchEvent(event)
    }

    // ---------------
    // LISTENNERS
    // ---------------
    bindAnimationEvent() {
        this.element.addEventListener('transitionend', this.events.transitionEnd, true)
    }

    bindMouseoverEvent() {
        this.element.addEventListener('mouseover', this.events.mouseOver, true)
        document.addEventListener('mousemove', this.events.mouseMove, true)
    }

    unBindAnimationEvent() {
        this.element.removeEventListener('transitionend', this.events.transitionEnd, true)
    }

    unBindMouseoverEvent() {
        this.element.removeEventListener('mouseover', this.events.mouseOver, true)
        document.removeEventListener('mousemove', this.events.mouseMove, true)
    }

    // ---------------
    // EVENT FUNCTIONS
    // ---------------
    transitionEnd(ev) {
        if (typeof ev.target.classList === 'undefined' || ev.target.classList === null) return
        if ([].indexOf.call(ev.target.classList, 'c-booking-widget__resume') >= 0) return
        this.element.classList.remove('c-booking-widget--shrink-tmp')
        this.unBindAnimationEvent()
        this.$.resume.classList.remove('show')
    }

    mouseOver() {
        if (this.isCollapsed) {
            this.dispatchEvent(true)
        }
        this.$.elementBody.classList.remove('animate')
        this.element.classList.add('c-booking-widget--shrink-tmp')
        this.element.classList.add('c-booking-widget--no-overflow')
        this.$.resume.classList.remove('show')
        this.isCollapsed = false
    }

    mouseMove(ev) {
        let path = []
        if (typeof ev.composedPath === 'function') {
            path = ev.composedPath()
            this.mouseIsOverElement = path.includes(this.element)
            return
        }

        path = []
        // eslint-disable-next-line prefer-destructuring
        let target = ev.target
        while (target.parentNode !== null) {
            path.push(target)
            target = target.parentNode
        }

        this.mouseIsOverElement = path.includes(this.element)
    }

    // ---------------
    // ANIMATION FUNCTIONS
    // ---------------
    emulateContainerHeight() {
        const node = document.createElement('div')
        node.style.height = `${this.elementHeight}px`
        node.setAttribute('class', 'c-booking-widget-temporal')
        this.element.insertAdjacentElement('beforebegin', node)
    }
}

export default FixBookingWidget
