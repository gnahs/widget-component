const webpack = require('webpack-boilerplate/webpack.config.js')

webpack.entry = {
    'booking-widget': 'js/src/js/entry-points/app.js',
    'widget-component': 'js/src/js/entry-points/demo.js',
}

module.exports = webpack
